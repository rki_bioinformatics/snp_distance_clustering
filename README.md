# Agglomerative single-linkage clustering for distance matrices in R
In agglomerative clustering samples are joined bottom-up in an iterative process.  
In this implementation samples are joined to clusters if their distance is less than a set threshold.  
Clusters are joined with a single-linkage approach: 
* The pair of samples (one of each cluster) with the minimum distance is identified.
* This pair's distance must be less than the threshold  
  
  
This implementation also works with relative pairwise distances where one sample can connect two very distant samples by small distances.
If those are used for clustering similar samples are clustered together but overall distance in clusters is large.
The user can choose if those cases should be considered in clustering or flagged and excluded.  
The exclusion process iteratively removes one flagged sample until no flagged sample was identified any more.
The latter choice gives more valid results for "transmission clusters".


## Usage


```
# distance_matrix: square matrix or data.frame with numeric distances
# threshold: numeric value, only distances less than this are considered for cluster merging
# check_transmission_validity:  TRUE or FALSE, if TRUE samples introducing large distances into clusters are flagged and removed
# return value: dataframe with each sample and assigned group, if sample does not belong to any cluster its tag is "ungrouped", flagged samples are tagged with "proxy"

source("agglomerative_clustering.R")
result_groups <- agglomerative_clustering(distance_matrix, threshold, check_transmission_validity)

```